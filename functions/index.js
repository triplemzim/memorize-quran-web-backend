const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

require('dotenv').config();

const stripe = require('stripe')(process.env.stripe_secret_key);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });


exports.createStripeSession = functions.https.onRequest(async (req, res) => {
    const { priceId } = req.body;

    // See https://stripe.com/docs/api/checkout/sessions/create
    // for additional parameters to pass.
    try {
      const session = await stripe.checkout.sessions.create({
        mode: "subscription",
        payment_method_types: ["card"],
        line_items: [
          {
            price: priceId,
            currency: 'USD',
            // For metered billing, do not pass quantity
            quantity: 1,
          },
        ],
        // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
        // the actual Session ID is returned in the query parameter when your customer
        // is redirected to the success page.
        success_url: 'http://localhost:8080?session_id={CHECKOUT_SESSION_ID}',
        cancel_url: 'http://localhost:8080',
      });
  
      res.send({
        sessionId: session.id,
      });
    } catch (e) {
      console.log(e);
      res.status(400);
      return res.send({
        error: {
          message: e.message,
        }
      });
    }
})

